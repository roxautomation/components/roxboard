#!/usr/bin/env python3
"""
 Fake data generator for ROX dashboard.

 Generates:
    - feedback topic
    - pose topic
    - logging text

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""


import asyncio
import random
import math
import logging
from logging.handlers import QueueHandler

# initial values for feedback and pose
FEEDBACK = {"angle": 0, "acceleration": 0, "velocity": 0}

FEEDBACK_DELAY = 5  # seconds
POSE_DELAY = 1  # seconds

LOG_TOPIC = "log"
FEEDBACK_TOPIC = "feedback"
POSE_TOPIC = "pose"

GPS_REF = (51.365952, 6.172037)


def circular_motion_generator(
    latlon: tuple[float, float], radius_meters: float, delta_angle: float
):
    """Generator for simulating circular motion around a center point, generating GPS coordinates."""
    center_lat, center_lon = latlon
    angle = 0.0  # Angle in radians

    while True:
        # Calculate new position
        delta_lat = radius_meters * math.cos(angle) / 111320
        delta_lon = (
            radius_meters
            * math.sin(angle)
            / (111320 * math.cos(math.radians(center_lat)))
        )

        new_lat = center_lat + delta_lat
        new_lon = center_lon + delta_lon

        # Calculate heading (direction of movement)
        heading = (math.degrees(angle) + 90) % 360

        yield {"lat": new_lat, "lon": new_lon, "heading": heading}

        # Increment angle
        angle += delta_angle


class FakeDataGenerator:
    """Class to generate fake data for the dashboard.

    * multiple loops generate data and put it into a queue.
    * the queue is read by the websocket server and sent to the client.
    """

    def __init__(self, queue: asyncio.Queue):
        """create a fake data generator.

        Args:
            queue (asyncio.Queue): queue to put data in
        """
        self._log = logging.getLogger("fake")
        self._stop_event = asyncio.Event()
        self._feedback: dict = FEEDBACK
        self._out_q = queue

        # logging handlers
        self._log_q: asyncio.Queue = asyncio.Queue()
        self._log_handler = QueueHandler(self._log_q)
        self._log_handler.setLevel(logging.DEBUG)
        self._log.addHandler(self._log_handler)

    async def main(self):
        """Simulate data source updates."""

        # TODO: keel list of tasks and cancel them on stop
        async with asyncio.TaskGroup() as tg:
            tg.create_task(self._generate_feedback())
            tg.create_task(self._generate_pose())
            tg.create_task(self._generate_logging())
            tg.create_task(self._handle_logging())

    async def _send(self, topic: str, data: dict | str):
        """put data in the queue."""
        await self._out_q.put([topic, data])

    async def _generate_feedback(self):
        """Generate a feedback signal."""
        while not self._stop_event.is_set():
            # --- angle
            self._feedback["angle"] += 1
            # wrap around at 360
            self._feedback["angle"] %= 360

            # --- acceleration
            # make acceleration random float between -1 and 1
            self._feedback["acceleration"] = random.uniform(-1, 1)

            # --- velocity
            # add acceleration to velocity
            self._feedback["velocity"] += self._feedback["acceleration"]

            await self._send(FEEDBACK_TOPIC, self._feedback)
            await asyncio.sleep(FEEDBACK_DELAY)

    async def _generate_pose(self):
        """Generate a ramp signal."""

        gen = circular_motion_generator(GPS_REF, 25, math.radians(1))

        while not self._stop_event.is_set():
            pose = next(gen)
            await self._send(POSE_TOPIC, pose)
            await asyncio.sleep(POSE_DELAY)

    async def _generate_logging(self):
        """generate logging strings"""
        idx = 0

        while not self._stop_event.is_set():
            self._log.debug(f"debug message {idx}")

            if idx % 5:
                self._log.info(f"info message {idx}")

            await asyncio.sleep(1)
            idx += 1

    async def _handle_logging(self):
        """handle logging messages"""
        formatter = logging.Formatter(
            fmt="%(asctime)s [%(name)s] %(levelname)s %(message)s", datefmt="%H:%M:%S"
        )

        while not self._stop_event.is_set():
            item = await self._log_q.get()

            msg = formatter.format(item)
            print(f"logging {msg}")
            await self._send(LOG_TOPIC, msg)
            self._log_q.task_done()

    async def stop(self):
        """Signal to stop updating data sources."""
        self._stop_event.set()
