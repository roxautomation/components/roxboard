# pylint: skip-file
# type: ignore
""" Simpe dashboard to visualize websocket data.

consists of these elements:

* a map to show the robot position
* a table to show feedback data
* a text area to show logging data

NOT included:

* live plotting of data

"""

from browser import document
from browser.html import TABLE, TR, TH, TD

from assets.map import Mappy
from assets.bridge import Bridge


print("Starting dashboard...")

GPS_REF = (51.365948, 6.172037)
FEEDBACK_FIELDS = ["angle", "acceleration", "velocity"]


def create_feedback_table(fields: list[str]):
    """create data table"""
    div = document["feedback-data"]
    table = TABLE()
    row = TR()
    row <= TH("Name")
    row <= TH("Value", Class="value-col")
    table <= row

    for f in fields:
        row = TR()
        row <= TD(f) + TD("-", id=f)
        table <= row

    div <= table


def handle_pose(msg):
    """handle pose data"""
    gps_map.move_robot((msg["lat"], msg["lon"]), msg["heading"])


def handle_feedback(data):
    """handle feedback data"""

    for k, v in data.items():
        if k in FEEDBACK_FIELDS:
            elm = document[k]
            elm.textContent = str(v)


def handle_logging(msg):
    """handle logging data"""
    elm = document["txt_log"]
    elm.value += str(msg) + "\n"

    # scroll to bottom
    elm.scrollTop = elm.scrollHeight


create_feedback_table(FEEDBACK_FIELDS)

gps_map = Mappy(latlon=GPS_REF, trace_length=500)
bridge = Bridge(loglevel=1)
bridge.connect(url="ws://localhost:8000/ws/data-stream")
bridge.subscribe("pose", handle_pose)
bridge.subscribe("feedback", handle_feedback)
bridge.subscribe("log", handle_logging)
