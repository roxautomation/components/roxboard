# pylint: skip-file
# type: ignore
from typing import Tuple, Optional, Dict
from collections import deque
from browser import alert, window

__version__ = "0.2.1"

print(f"importing mappy version {__version__}")

leaflet = window.L


def svgIcon():
    """create svg icon for robot"""
    icon = leaflet.divIcon(
        {
            "html": """<svg
  width="24"
  height="40"
  viewBox="0 0 100 100"
  version="1.1"
  preserveAspectRatio="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path d="M0 100 L50 0 L100 100 Z" fill="#FF2A2A"></path>

  <path d="M0 100 100 100" style="stroke:#000000;stroke-width:5"/>
  <path d="M 50  75 V 100" style="stroke:#000000;stroke-width:5"/>
</svg>""",
            "className": "",
            "iconSize": [24, 40],
            "iconAnchor": [12, 40],
        }
    )
    return icon


class Mappy:
    def __init__(self, latlon, tileserver=None, trace_length: int = 100):
        """create a map, use geolocation if latlon is None"""

        M = leaflet.map("map")
        self.M = M
        self._center_marker = None

        self.gps_ref = latlon
        M.setView(latlon, 20)
        self._center_marker = leaflet.marker(latlon).addTo(M)  # measurement marker

        if tileserver is not None:
            tile_params = {"maxZoom": 22}
            leaflet.tileLayer(tileserver, tile_params).addTo(M)
        else:
            leaflet.tileLayer.wms(
                "https://service.pdok.nl/hwh/luchtfotorgb/wms/v1_0",
                {"layers": "2022_orthoHR", "maxZoom": 23},
            ).addTo(M)

        M.on("click", self.click_feedback)

        self.marker = None

        self.latlon_input = None  # latlon selected on the map

        self._robot = None

        self._markers = {}

        self._geojson = {}

        self._input_txt = None  # input field in ui
        self._path_markers = []
        self._target_path = None

        if trace_length > 0:
            self._history = deque(maxlen=trace_length)
            self._path = leaflet.polyline(self._history, {"color": "#02b3ca"}).addTo(M)
        else:
            self._history = None

    def set_input_txt(self, obj):
        """connect input field"""
        self._input_txt = obj

    def add_geojson(self, geojson, id, style=None):
        """add geojson to map"""
        if id in self._geojson:
            self._geojson[id].remove()

        if style is None:
            layer = leaflet.geoJSON(geojson).addTo(self.M)
        else:
            layer = leaflet.geoJSON(geojson, {"style": style}).addTo(self.M)
        self._geojson[id] = layer

    def _set_geolocation(self, pos):
        """callback for geolocation"""
        xyz = pos.coords
        lat = xyz.latitude
        lon = xyz.longitude
        latlon = (lat, lon)
        print(f"Got geolocation {latlon}")
        self.M.setView(latlon, 12)
        self._center_marker = leaflet.marker(latlon).addTo(self.M)

    def click_feedback(self, evt):
        txt = f"({evt.latlng.lat:.8f},{evt.latlng.lng:.8f})"
        print(txt)
        if self._center_marker is None:
            self._center_marker = leaflet.Marker(evt.latlng)

        leaflet.popup().setLatLng(evt.latlng).setContent(txt).openOn(self.M)
        self.latlon_input = [evt.latlng.lat, evt.latlng.lng]

        # auto close
        # timer.set_timeout(lambda: self.M.closePopup(), 500)

        if self._input_txt is not None:
            self._input_txt.value = ",".join([str(v) for v in self.latlon_input])

    def add_marker(self, latlon: Tuple, txt: str = ""):
        """add a simple marker with popup txt"""
        marker = leaflet.marker(latlon).bindPopup(txt)
        self.M.addLayer(marker)

    def set_marker(self, name: str, latlon: Optional[Tuple] = None, close_popup=True):
        """set marker latlon by name. latlon will be self.latlon_input"""

        if latlon is None:  # use self.latlon_input
            latlon = self.latlon_input

        if latlon is None:
            alert("No input yet")
            return

        print(f"Setting marker {name} to {latlon}")

        html = f'<div class="map-label"><div class="map-label-content">{name}</div><div class="map-label-arrow"></div></div>'  # noqa

        try:
            if name not in self._markers:
                icon = leaflet.divIcon(
                    {
                        "iconSize": None,
                        "html": html,
                    }
                )
                print("creating new marker")
                self._markers[name] = (
                    leaflet.marker(latlon, {"icon": icon, "draggable": True})
                    .addTo(self.M)
                    .on("click", lambda evt, name=name: self.get_marker_latlon(name))
                )

                # leaflet.marker(latlon).addTo(self.M)  # check marker
            else:
                self._markers[name].setLatLng(latlon)
        except TypeError as err:
            print(f"Error setting marker {err=}")

        except Exception as err:
            alert(f"Could not set marker: {err}")

        if close_popup:
            self.M.closePopup()

    def get_marker_latlon(self, name):
        """set latlon_input to latlon of a marker"""

        assert (
            name in self._markers
        ), f"Marker {name} not set, markers: {self._markers.keys()}"
        coords = self._markers[name].getLatLng()

        self.latlon_input = (coords.lat, coords.lng)
        print(f"Input set {self.latlon_input=}")

    def get_abcd(self) -> Dict:
        """get abcd points"""

        out = {}

        for name in "ABCD":
            marker = self._markers[name]
            assert name in self._markers, f"Marker {name} not set"
            coords = marker.getLatLng()
            out[name] = (coords.lat, coords.lng)

        return out

    def plot_ab(self):
        """plot ab line"""
        latlon = []

        try:
            for name in "AB":
                coords = self._markers[name].getLatLng()
                latlon.append((coords.lat, coords.lng))
        except KeyError:
            print("error - AB markers not set.")

        print(latlon)

        self.set_target_path(latlon)

    def plot_path(self, wps):
        """plot path with get_path from api"""

        # remove present markers
        for marker in self._path_markers:
            self.M.removeLayer(marker)

        # add markers
        for idx, wp in enumerate(wps):
            icon = leaflet.divIcon(
                {
                    "iconSize": (25, 15),
                    "className": "leaflet-div-icon",
                    "html": str(idx),
                }
            )
            marker = leaflet.marker(wp["latlon"], {"icon": icon}).bindPopup(
                str(wp["mods"])
            )
            self.M.addLayer(marker)
            self._path_markers.append(marker)

        latlon = [wp["latlon"] for wp in wps]
        self.set_target_path(latlon)

    def set_target_path(self, latlon: list):

        params = {
            "delay": 2000,
            "dashArray": [2, 100],
            "weight": 5,
            "color": "#0000FF",
            "pulseColor": "#FFFFFF",
            "paused": False,
            "reverse": False,
            "hardwareAccelerated": True,
        }
        print("drawing path")
        latlongs = [leaflet.latLng(lat, lon) for lat, lon in latlon]
        if self._target_path is None:
            self._target_path = leaflet.polyline.antPath(latlongs, params).addTo(self.M)
        else:
            self._target_path.setLatLngs(latlongs)

    def move_robot(self, latlon, heading):

        if self._robot is None:
            # create robot
            self._robot = leaflet.marker(
                latlon, {"icon": svgIcon(), "rotationAngle": heading}
            )
            self._robot.addTo(self.M)
            self.M.setView(latlon, 18)

        if self._history is not None:
            self._history.append(latlon)
            latlongs = [leaflet.latLng(lat, lon) for lat, lon in self._history]
            self._path.setLatLngs(latlongs)

        self._robot.setLatLng(latlon)

        self._robot.setRotationAngle(heading)
