# pylint: skip-file
# type: ignore
from typing import Optional

# connection to webbridge
import javascript
from browser import websocket, window, timer, document


def test_fcn():
    print("this is a test function.")


class Logger:
    """simple logging class
    logging levels : 0 - error, 1- info  2- debug

    """

    def __init__(self, name, level=1):
        self.name = name
        self.level = level

    def debug(self, msg):
        if self.level > 1:
            print(f"[{self.name}] - DEBUG - {msg}")

    def info(self, msg):
        if self.level > 0:
            print(f"[{self.name}] - INFO - {msg}")

    def error(self, msg):
        print(f"[{self.name}] - ERROR - {msg}")


class Bridge:
    """web bridge"""

    def __init__(self, name="bridge", loglevel=1, conn_status_id="conn-status"):
        self._ws = None
        self._connected = False
        self._sub_callbacks = {}  # subscription handlers
        self._service_callbacks = {}  # service handlers

        self._conn_status_id = conn_status_id  # id of status element

        self.debug_ignore = []  # ignored debug topics

        self.log = Logger(name, loglevel)

    def connect(self, url: Optional[str] = None):
        """connect to websocket server"""
        if url is None:
            url = f"ws://{window.location.hostname}:8000"

        self.log.info(f"Connecting to {url}")

        self._ws = websocket.WebSocket(url)
        for evt, fcn in [
            ("open", self._open),
            ("close", self._close),
            ("message", self._message),
            ("error", self._error),
        ]:
            self._ws.bind(evt, fcn)

    def disconnect(self):
        if self._connected:
            self._ws.close()

    def subscribe(self, topic, callback):
        assert (
            topic not in self._sub_callbacks
        ), "currently only one handler per topic is allowed"
        self.log.info(f"subscribing to {topic=}")

        self._sub_callbacks[topic] = callback

        self.log.info(f"Current subscribers: {self._sub_callbacks.keys()}")

    def publish(self, topic, msg):
        """publish a message to a topic"""

        cmd = {"topic": topic, "data": msg}
        self._send(cmd)

    def _send(self, data: dict):
        """send data to socket"""

        json = javascript.JSON.stringify(data)
        self.log.debug(f"sending {json=}")

        if not self._connected:
            self.log.info("not connected yet...")
            timer.set_timeout(self._send, 500, data)
            return

        self._ws.send(json)

    def _error(self, evt):
        self.log.error(f"Socket error {evt}")
        self._set_status_color("red")

    def _open(self, _):
        self._connected = True
        self.log.info("connected.")
        self._set_status_color("green")

    def _message(self, message):
        self.log.debug(f"got message {message.data}")

        try:
            topic, data = javascript.JSON.parse(message.data)

            if topic not in self._sub_callbacks.keys():
                return

            cbk = self._sub_callbacks[topic]
            self.log.debug(f"calling {cbk.__name__} args: {data}")
            cbk(data)
        except Exception as error:
            self.log.error(f"{error.__class__.__name__}:{error}")

    def _close(self, _):
        self.log.info("connection closed")
        self._set_status_color("red")

    def _set_status_color(self, color):
        """set status element to a color"""
        try:
            document[self._conn_status_id].style.color = color
        except:  # noqa
            self.log.error("error changing color")
