#!/usr/bin/env python3
"""
 Backend server for ROX dashboard.

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import asyncio
from contextlib import asynccontextmanager
from pathlib import Path

import time
import logging
import yaml  # type: ignore
from fastapi import FastAPI, HTTPException, WebSocket
from fastapi.responses import FileResponse, JSONResponse
from fastapi.staticfiles import StaticFiles

from roxboard.fake_data import FakeDataGenerator

logging.basicConfig(level=logging.DEBUG)


@asynccontextmanager
async def app_lifespan(_):  # pylint: disable=unused-argument
    """Context manager to run a task during the lifespan of the app."""

    task = asyncio.create_task(fake_data.main())  # pylint: disable=unused-variable
    try:
        yield
    except asyncio.CancelledError:
        pass  # Gracefully handle the CancelledError
    finally:
        await fake_data.stop()
        # await task   # may hang here if one of the tasks is waiting for q queue.


ROOT = Path(__file__).parent.absolute()  # path to the root of the app
CFG_PATH = ROOT / "config"  # path to dashboard definitions

app = FastAPI()
app.router.lifespan_context = app_lifespan

# Mounting the static file directory
app.mount("/assets", StaticFiles(directory=ROOT / "site/assets"), name="assets")
app.mount("/css", StaticFiles(directory=ROOT / "site/css"), name="css")

data_queue: asyncio.Queue = asyncio.Queue()
fake_data = FakeDataGenerator(data_queue)


def load_config(dashboard_name: str):
    """Load configuration from a YAML file."""

    # get current path

    config_path = CFG_PATH / f"{dashboard_name}.yaml"
    assert config_path.exists(), "Config file not found"
    cfg = yaml.safe_load(config_path.open("r", encoding="utf-8"))
    return cfg


def save_config(dashboard_name: str, settings: dict):
    # Logic to save the configuration for the given dashboard
    # TODO: implement
    pass


# ------------------ API Endpoints ------------------#


@app.get("/")
async def main_page():
    """main page"""
    # TODO: replace with a documentation page during development and a default dashboard later
    return FileResponse(ROOT / "site/index.html")


@app.get("/proto")
async def proto_page():
    """brython demo dashboard"""
    return FileResponse(ROOT / "site/brython_dash.html")


@app.get("/api/settings")
def get_app_settings():
    """application settings and list of dashboards"""
    config_names = [file.stem for file in CFG_PATH.glob("*.yaml")]
    return {"startup_dashboard": None, "dashboards": config_names}


@app.get("/api/settings/{dashboard_name}")
async def get_settings(dashboard_name: str):
    """REST API endpoint to get settings for a specific dashboard."""
    try:
        settings = load_config(dashboard_name)
    except FileNotFoundError as exc:
        raise HTTPException(status_code=404, detail="Dashboard not found") from exc

    return JSONResponse(content=settings)


@app.post("/api/settings/{dashboard_name}")
async def update_settings(dashboard_name: str, new_settings: dict):
    """REST API endpoint to update settings for a specific dashboard."""
    try:
        # Update settings logic here, e.g., save to a file or database
        save_config(dashboard_name, new_settings)
    except Exception as exc:
        raise HTTPException(status_code=500, detail="Error updating settings") from exc

    return {"status": "success", "updated_settings": new_settings}


@app.websocket("/ws/data-stream")
async def websocket_endpoint(websocket: WebSocket):
    """WebSocket connection for real-time data streaming."""
    await websocket.accept()
    while True:
        try:
            data = await data_queue.get()
            data.append(time.time())
            await websocket.send_json(data)
            data_queue.task_done()

        except Exception:  # pylint: disable=broad-except
            # Handle disconnection or error
            break


def main():
    """Run the app."""
    import uvicorn  # pylint: disable=import-outside-toplevel

    uvicorn.run(app, host="0.0.0.0", port=8000)


if __name__ == "__main__":
    main()
