#!/bin/bash

IMG_NAME="local/roxboard"

# copy source code
cp -r roxboard docker/src

# build docker image
docker build -t $IMG_NAME -f ./docker/Dockerfile.app  ./docker

# run docker image
docker run --rm -p 8000:8000 $IMG_NAME
