# roxboard



Dashboard for robotics development


# Microservice

## Quick start

Run the app from gitlab image

```
docker pull registry.gitlab.com/roxautomation/components/roxboard:app
docker run --rm -p 8000:8000 registry.gitlab.com/roxautomation/components/roxboard:app
```

Build and run locally

`build_and_run.sh`


**demo pages**

* [index](http://localhost:8000) - Display raw socket data
* [example dashboard](http://localhost:8000/proto) - prototype dashboard written in Brython
* [api docs](http://localhost:8000/docs)


## System overview

This system is built using FastAPI, providing a high-performance, easy-to-use framework that supports asynchronous operations and is ideal for building robust APIs and real-time data streaming services.

- **Dashboard Configuration Management**: It allows users to interact with different dashboard configurations.
  - start container and see [api docs](http://localhost:8000/docs) for reference.

- **Real-time Data Streaming**: Through a WebSocket endpoint (`/ws/data-stream`), it streams real-time data to clients. This is particularly useful for dashboards that need to display up-to-date information.

- **Lifecycle Management**: The server uses an async context manager to handle tasks throughout the app's lifespan, ensuring graceful startup and shutdown processes.

- **Data Generation and Handling**: It integrates a `FakeDataGenerator` to simulate data streams, which are then queued and sent to clients via the WebSocket connection.
This will later be replaced by a real data from a `roxbot` system

- **File Serving**: The root endpoint (`/`) serves a static HTML file, which can be replaced with a documentation page during development and a default dashboard in the production stage.




## What goes where
* `roxboard` app code. It is *not* a python package. Needs to be added to `PYTHONPATH`.
* `docker` folder contains dockerfiles for building base system and app images.
* `.gitlab-ci.yml` takes care of the building steps.


**Note:** system image is built manually to avoid overcomplicated ci rules. Execute
build once to enable app builds.
